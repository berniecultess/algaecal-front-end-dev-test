// import {ProductBundles} from "./modules/ProductBundles";
import './styles/main.scss';
import loadSVGs from './modules/svg-replace';
import 'popper.js';
import 'bootstrap';
import $ from 'jquery';


document.addEventListener('DOMContentLoaded', () => {
  loadSVGs();
});

$(document).ready(function () {
  $('.speak-to-our-bone-specialists').removeClass("d-md-block");

  // Used to open and display information in modal
  $('.modalInfo').click(function () {
    $.ajax({
      url: '/assets/scripts/options.json',
      dataType: 'json',
      type: 'post',
      data: '',
      success: function (response) {
        let content = response.acf['7yr_full_copy'];
        console.log(content);
        // Add response in Modal body
        $('.modal-body .container').html(content);

        // Display Modal
        $('#sevenYearGuaranteeModal').modal('show');
      }
    });
  });

  // Used to determine day and times of business, if values are TRUE show "Speak to our Bone Health Consultants!", 
  // else hide message
  $.getJSON('/assets/scripts/options.json', function (response) {
    let hrsInfo = response.acf['office_hours'];
    let d = new Date();
    let day = d.getDay();
    let hour = d.getHours();
    let json_day, json_st_time, json_cl_time;

    console.info("type of hour: ", typeof hour);
    console.info("TODAY: ", day);
    console.log(day, hour);

    json_day = Number((hrsInfo[day].day === 7) ? 0 : hrsInfo[day].day);
    json_st_time = Number(hrsInfo[day].starting_time.substring(0, 2));
    json_cl_time = Number(hrsInfo[day].closing_time.substring(0, 2));


    console.log("DAY: ", day, "JS DAY", json_day, "TIME: ", hour, "START TIME: ", json_st_time, "CLOSE TIME:", json_cl_time);
    console.log("json_day === day: ", json_day === day, "hour >= st_time: ", hour >= json_st_time, "hour < cl_time:", hour < json_cl_time);

    console.log(typeof json_day, typeof hour, typeof json_st_time, typeof json_cl_time);

    console.log(hour + json_st_time);

    if (json_day === day) {
      if ((hour >= json_st_time) && (hour < json_cl_time)) {
        $('.speak-to-our-bone-specialists').addClass("d-md-block");
      }
    }

  });
});
