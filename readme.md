# AlgaeCal Web Developer Test

I want to first say thank you for giving me this chance to take this test and show you guys what I am capable of.

I had a bit an issue to get my bitbucket up and running, but I good to go now.

I used existing css rules to style some of the changes and I created new rules where I felt needed it.

To parse the JSON file, I did use jQuery. Bootstrap modals are very difficult to control without jQuery. 

I was not able to commit all of my files. I had to manually edit, create files via bitbucket.
These files are:
src/index.js
src/assets/scripts/options.json

I hope that you guys can bring it down when you clone the project. If not, then the code won't work.

Again, I want to thank you all and I look forward to hearing from you guys in the next couple of days.

Bernie
